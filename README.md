# Algorithms

This is my collection of Algorithms, which I will try to implement in 
Python.

I'm following the book [Essential Algorithms](http://eu.wiley.com/WileyCDA/WileyTitle/productCd-1118612108.html)
by Rod Stephens.
