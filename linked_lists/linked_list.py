class LinkedList():
    def __init__(self, value, next_item):
        self.value = value
        self.next_item = next_item

def find_cell_before(top, target):
    while (top.next_item != None):
        if (top.next_item.value == target):
            return top
        top = top.next_item
    return None

# initiating a new linked list

first = LinkedList(0, None)
curr = first

for x in range(10):
    curr.next_item = LinkedList(x, None)
    curr = curr.next_item
    
# finding the cell before a target value
# with a sentintel value created before
# not sure if this is correct though

# This implementation has a runtime of
# O(n) because in the worst case, it has
# to search the whole linked list to find
# the target

def find_cell_before_sentinel(top, target):
    while (top.next_item != None):
        if (top.next_item.value == target):
            return top
        top = top.next_item

    return None

# print(find_cell_before_sentinel(first, 4).value)

# Adding cells after the sentinel

def add_cell_at_beginning(top, new_cell):
    new_cell.next_item = top.next_item
    top.next_item = new_cell

# Adding cells at the end

def add_cell_at_the_end(top, new_cell):
    while (top.next_item != None):
        top = top.next_item
    top.next_item = new_cell
    new_cell.next_item = None
